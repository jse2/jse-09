package ru.mirsaitov.tm.constant;

public class TerminalConst {
    public static final String SPLIT = " ";
    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW = "project-view";
    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PROJECT_UPDATE = "project-update";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_REMOVE = "task-remove";
    public static final String TASK_UPDATE = "task-update";

    public static final String OPTION_INDEX = "index";
    public static final String OPTION_NAME = "name";
    public static final String OPTION_ID = "id";
}