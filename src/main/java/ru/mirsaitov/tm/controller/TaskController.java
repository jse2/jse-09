package ru.mirsaitov.tm.controller;

import ru.mirsaitov.tm.constant.TerminalConst;
import ru.mirsaitov.tm.entity.Task;
import ru.mirsaitov.tm.service.TaskService;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Create task
     *
     * @param arguments - arguments of command
     */
    public void createTask(final String[] arguments) {
        final String name = arguments.length > 0 ? arguments[0] : "";
        final String description = arguments.length > 1 ? arguments[1] : "";
        if (description == null) {
            taskService.create(name);
        } else {
            taskService.create(name, description);
        }
        System.out.println(bundle.getString("taskCreate"));
    }

    /**
     * Clear tasks
     */
    public void clearTask() {
        taskService.clear();
        System.out.println(bundle.getString("taskClear"));
    }

    /**
     * View task
     *
     * * @param arguments - arguments of command
     */
    public void viewTask(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskService.findByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskService.findByName(param);
                break;
            case TerminalConst.OPTION_ID:
                task = taskService.findById(Long.parseLong(param));
                break;
        }
        displayTask(task);
    }

    /**
     * Display task
     *
     * @param task task
     */
    public void displayTask(Task task) {
        if (task == null) {
            System.out.println(bundle.getString("notFound"));
            return;
        }

        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    /**
     * Remove task
     *
     * * @param arguments - arguments of command
     */
    public void removeTask(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        if (option == null || param == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                task = taskService.removeByIndex(Integer.parseInt(param) - 1);
                break;
            case TerminalConst.OPTION_NAME:
                task = taskService.removeByName(param);
                break;
            case TerminalConst.OPTION_ID:
                task = taskService.removeById(Long.parseLong(param));
                break;
        }
        displayTask(task);
    }

    /**
     * Update task
     *
     * * @param arguments - arguments of command
     */
    public void updateTask(final String[] arguments) {
        final String option = arguments.length > 0 ? arguments[0] : null;
        final String param = arguments.length > 1 ? arguments[1] : null;
        final String name = arguments.length > 2 ? arguments[2] : null;
        final String description = arguments.length > 3 ? arguments[3] : null;
        if (option == null || param == null || name == null) {
            System.out.println(bundle.getString("commandSyntaxError"));
            return;
        }
        Task task = null;
        switch (option) {
            case TerminalConst.OPTION_INDEX:
                if (description == null) {
                    task = taskService.updateByIndex(Integer.parseInt(param) - 1, name);
                } else {
                    task = taskService.updateByIndex(Integer.parseInt(param) - 1, name, description);
                }
                break;
            case TerminalConst.OPTION_ID:
                if (description == null) {
                    task = taskService.updateById(Long.parseLong(param), name);
                } else {
                    task = taskService.updateById(Long.parseLong(param), name, description);
                }
                break;
        }
        displayTask(task);
    }

    /**
     * List tasks
     */
    public void listTask() {
        int index = 0;
        for (final Task task : taskService.findAll()) {
            System.out.println("INDEX: " + index++ + " ID: " + task.getId() + " " + task.getName() + ": " + task.getDescription());
        }
    }

}
