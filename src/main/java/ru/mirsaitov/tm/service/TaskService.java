package ru.mirsaitov.tm.service;

import ru.mirsaitov.tm.entity.Task;
import ru.mirsaitov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return taskRepository.create(name);
    }

    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (description == null || description.isEmpty()) {
            return null;
        }
        return taskRepository.create(name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task findByIndex(int index) {
        if (index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        return taskRepository.findByIndex(index);
    }

    public Task findByName(String name) {
        if (name == null) {
            return null;
        }
        return taskRepository.findByName(name);
    }

    public Task findById(Long id) {
        if (id == null) {
            return null;
        }
        return taskRepository.findById(id);
    }

    public Task removeByIndex(int index) {
        if (index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        return taskRepository.removeByIndex(index);
    }

    public Task removeByName(String name) {
        if (name == null) {
            return null;
        }
        return taskRepository.removeByName(name);
    }

    public Task removeById(Long id) {
        if (id == null) {
            return null;
        }
        return taskRepository.removeById(id);
    }

    public Task updateByIndex(int index, String name) {
        if (index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        return taskRepository.updateByIndex(index, name);
    }

    public Task updateByIndex(int index, String name, String description) {
        if (index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (description == null || description.isEmpty()) {
            return null;
        }
        return taskRepository.updateByIndex(index, name, description);
    }

    public Task updateById(Long id, String name) {
        if (id == null) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        return taskRepository.updateById(id, name);
    }

    public Task updateById(Long id, String name, String description) {
        if (id == null) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        if (description == null || description.isEmpty()) {
            return null;
        }
        return taskRepository.updateById(id, name, description);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

}
