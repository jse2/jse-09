package ru.mirsaitov.tm;

import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Scanner;

import ru.mirsaitov.tm.constant.TerminalConst;
import ru.mirsaitov.tm.controller.ProjectController;
import ru.mirsaitov.tm.controller.SystemController;
import ru.mirsaitov.tm.controller.TaskController;
import ru.mirsaitov.tm.repository.ProjectRepository;
import ru.mirsaitov.tm.repository.TaskRepository;
import ru.mirsaitov.tm.service.ProjectService;
import ru.mirsaitov.tm.service.TaskService;

/**
 * Task-manager application
 *
 * @author Mirsaitov Grigorii
 */
public class Application {

    private final ResourceBundle bundle = ResourceBundle.getBundle("MessagesBundle");

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskRepository taskRepository = new TaskRepository();

    private final TaskService taskService = new TaskService(taskRepository);

    private final TaskController taskController = new TaskController(taskService);

    private static final SystemController systemController = new SystemController();

    {
        projectService.create("1", "2");
        taskService.create("3", "4");
    }

    /**
     * Entry point of programm
     */
    public static void main(final String[] args) {
        final Application app = new Application();
        systemController.displayWelcome();
        app.run();
    }

    /**
     * Main loop of program
     */
    public void run() {
        Scanner scanner = new Scanner(System.in);
        String command;
        while (scanner.hasNextLine()) {
            command = scanner.nextLine();
            if (!process(command)) {
                break;
            }
        }
    }

    /**
     * Process of input parameter
     *
     * @param line - command for execute
     * @return true - wait next parameter, false - exit programm
     */
    public boolean process(final String line) {
        if (line == null || line.isEmpty()) {
            return true;
        }

        if (TerminalConst.CMD_EXIT.equals(line)) {
            return false;
        }

        processCommand(line);
        return true;
    }

    /**
     * Print to System.out result of parameter
     *
     * @param line - command and parameter
     */
    private void processCommand(final String line) {
        final String parts[] = line.split(TerminalConst.SPLIT);
        final String command = parts[0];
        final String[] arguments = Arrays.copyOfRange(parts, 1, parts.length);
        switch (command) {
            case TerminalConst.CMD_VERSION:
                systemController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                systemController.displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                systemController.displayHelp();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject(arguments);
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.listProject();
                break;
            case TerminalConst.PROJECT_VIEW:
                projectController.viewProject(arguments);
                break;
            case TerminalConst.PROJECT_REMOVE:
                projectController.removeProject(arguments);
                break;
            case TerminalConst.PROJECT_UPDATE:
                projectController.updateProject(arguments);
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask(arguments);
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.listTask();
                break;
            case TerminalConst.TASK_VIEW:
                taskController.viewTask(arguments);
                break;
            case TerminalConst.TASK_REMOVE:
                taskController.removeTask(arguments);
                break;
            case TerminalConst.TASK_UPDATE:
                taskController.updateTask(arguments);
                break;
            default:
                systemController.displayStub(line);
                break;
        }
    }

}