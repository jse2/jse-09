package ru.mirsaitov.tm.service;

import org.junit.Test;
import ru.mirsaitov.tm.entity.Task;
import ru.mirsaitov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TaskServiceTest {

    private final String name = "name";

    private final String description = "description";

    private final TaskService taskService = new TaskService(new TaskRepository());

    @Test
    public void shouldCreate() {
        Task task = taskService.create(name);
        assertEquals(name, task.getName());
        assertEquals(name, task.getDescription());

        task = taskService.create(name, description);
        assertEquals(name, task.getName());
        assertEquals(description, task.getDescription());
    }

    @Test
    public void shoudlClear() {
        Task project = taskService.create(name);
        assertTrue(!taskService.findAll().isEmpty());

        taskService.clear();
        assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void shouldFindAll() {
        List<Task> projects = new ArrayList<>();

        projects.add(taskService.create(name));
        assertTrue(projects.equals(taskService.findAll()));

        projects.add(taskService.create(name, description));
        assertTrue(projects.equals(taskService.findAll()));
    }

    @Test
    public void shouldFind() {
        Task task = taskService.create(name, description);

        assertEquals(taskService.findById(task.getId()), task);
        assertEquals(taskService.findByName(task.getName()), task);
        assertEquals(taskService.findByIndex(0), task);

        assertEquals(taskService.findById(1L), null);
        assertEquals(taskService.findByName("1"), null);
        assertEquals(taskService.findByIndex(1), null);
    }

    @Test
    public void shouldRemove() {
        Task task = taskService.create(name, description);
        assertEquals(taskService.removeById(task.getId()), task);

        task = taskService.create(name, description);
        assertEquals(taskService.removeByName(task.getName()), task);

        task = taskService.create(name, description);
        assertEquals(taskService.removeByIndex(0), task);

        assertEquals(taskService.removeById(1L), null);
        assertEquals(taskService.removeByName("1"), null);
        assertEquals(taskService.removeByIndex(1), null);
    }

    @Test
    public void shouldUpdate() {
        final String updateName = "9";
        final String updateDescription = "10";
        Task task = taskService.create(name, description);

        assertEquals(taskService.updateByIndex(0, updateName), task);
        assertEquals(task.getName(), updateName);

        assertEquals(taskService.updateById(task.getId(), updateDescription, updateName), task);
        assertEquals(task.getName(), updateDescription);
        assertEquals(task.getDescription(), updateName);
    }

}