# 1. Project information

Task manager application

# 2. Software requirements

Java 11, OS Windows

# 3. Technological stack

Java, Maven

# 4. Installation

## 4.1 Build project

```cmd
mvn clean install
```

## 4.2 Run application

```cmd
java -jar task-manager-1.0.9.jar
```

# 5. Supported commands

```cmd
version - Display program version.
about - Display developer info.
help - Display list of terminal commands.
project-create [name] [description] - Create project.
project-clear - Clear projects.
project-list - Display list of projects.
project-view {index|name|id} param - Display project by index, name or id.
project-remove {index|name|id} param - Remove project by index, name or id.
project-update {index|id} param name [description] - Update project by index or id.
task-create [name] [DESCRIPTION] - Create task.
task-clear - Clear tasks.
task-list - Display list of tasks.
task-view {index|name|id} param - Display task by index, name or id.
task-remove {index|name|id} param - Remove task by index, name or id.
task-update {index|id} param name [description] - Update task by index or id.
exit - Exit. 
```

# 6. Contact information

Grigorii Mirsaitov (mirsaitov_gr@nlmk.com)